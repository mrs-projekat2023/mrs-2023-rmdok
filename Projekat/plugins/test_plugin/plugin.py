from plugin_framework.extension import Extension
from .widget import TextEdit
from PySide6 import QtWidgets

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget = None
        self.iface = iface
        print("INIT TEST")

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        self.widget = TextEdit(self.iface.centralWidget())
        self.iface.setCentralWidget(self.widget)
        print("Activated")

    def deactivate(self):
        # TODO: ukloniti TextEdit sa centralnog widget-a.
        self.iface.takeCentralWidget()
        # self.iface.setCentralWidget(QtWidgets.QWidget(self.iface))
        print("Deactivated")
